#!/bin/bash
set -e
node_modules/.bin/json5 -c package.json5
rpl XXX $(git describe --tags --match="v*" | sed -e 's@-\([^-]*\)-\([^-]*\)$@+\1.\2@;s@^v@@;s@%@~@g') package.json
npm run prepare
